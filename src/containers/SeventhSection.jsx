import React from 'react'
import '../styles/seventhSection.scss'

export const SeventhSection = () => {
    return (
        <div className="sevenContainer">
            <img src={"./aset/Union.png"} alt="logo" />
            <div className="infoContainer">
                <div className="information">
                    <div className="ikuti">
                        <label>Ikuti kami</label>
                        <div style={{ display: 'flex', flexDirection: 'column', marginTop: 13 }}>
                            <span>Terms of Use</span>
                            <span>Privacy</span>
                            <span>Careers</span>
                            <span>About</span>
                            <span>CA Supply Chains Act</span>
                            <span>Sustainability</span>
                            <span>Affiliatesy</span>
                            <span>Recall Info and Diversity</span>
                        </div>
                    </div>
                    <div className="hubungi">
                        <label>Hubungi kami</label>
                        <div style={{ display: 'flex', flexDirection: 'column', marginTop: 13 }}>
                            <span style={{ fontWeight: 'bold' }}>Alamat</span>
                            <span>
                                Jl. Prof. Herman Yohanes No.1212,<br />
                                Terban, Kec. Gondokusuman, Kota Yogyakarta,<br />
                                Daerah Istimewa Yogyakarta 55223
                            </span>
                            <span style={{ fontWeight: 'bold', marginTop: 15 }}>Jam buka</span>
                            <span>
                                Mon - Fri 6:00 am - 8:00 pm Sat <br />
                                Sun 9:30 am - 6:00 pm
                            </span>
                        </div>
                    </div>
                </div>
                <div className="download">
                    <label>Download DamoGo App</label>
                    <div style={{ display: 'flex', marginTop: '37px' }}>
                        <img src={"./aset/app-store.png"} alt="app-store" style={{ width: "159.03px", height: "48px", marginRight: "24.25px" }} />
                        <img src={"./aset/google-play-1.png"} alt="google-store" style={{ width: "159.03px", height: "48px" }} />
                    </div>
                </div>
            </div>
        </div>
    )
}