import React from 'react'
import { Button } from '../components/utils'
import '../styles/sixthSection.scss'

export const SixthSection = () => {
    return (
        <div className="sixthContainer">
            <div className="text-comp">
                <span className="damogo">D A M O G O</span>
                <span className="bold-title">Untuk Supplier</span>
                <span className="detail-text">
                    Baik Anda adalah pertanian milik keluarga atau distributor nasional, platform khusus kami akan mengubah cara Anda berbisnis. Hemat waktu untuk tugas manual, kurangi kesalahan pesanan, dan jual lebih banyak produk.
                </span>
                <Button styles={{ marginTop: "58px" }} color="#005844">Pelajari selengkapnya</Button>
            </div>
            <div className="image-comp">
                <img className="phone" src={"./aset/damogo4.png"} alt="display" />
                <img className="ellipse" src={"./aset/ellipse 15.png"} alt="elipse" />
            </div>
        </div>
    )
}