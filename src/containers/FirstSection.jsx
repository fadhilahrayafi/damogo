import React, { Fragment, useState } from 'react'
import { Button } from '../components/utils'
import '../styles/firstSection.scss'

export const FirstSection = () => {
    const menu = ["Beranda", "Restoran", "Supplier"]
    const [active, setActive] = useState('Beranda')

    return (
        <Fragment>
            <img src="./aset/Ellipse 8.png" alt="blush" className="blush" style={{ zIndex: 1 }} />
            <div className="firstContainer" >
                <div className="header-comp">
                    <img src="./aset/Union.png" alt="logo" className="logo" />
                    <div className="menu">
                        {menu.map(item => {
                            return (
                                <span onClick={() => setActive(item)} key={item} className={active === item ? "active" : ""}>{item}</span>
                            )
                        })}
                    </div>
                </div>
                <div className="content">
                    <div className="text-comp">
                        <span className="title-text">Create taste, not <span style={{ color: "#009673" }}>waste</span></span>
                        <span className="detail">Selamatkan makanan lezat, makanan tidak terjual dari restaurant favoritmu, toko, dan pertanian agar tidak terbuang sia-sia serta dapatkan harga hemat hingga 90% dari harga reguler!</span>
                        <div style={{ display: 'flex', alignItems: "center" }}>
                            <Button color="#00A3F4" styles={{ width: "192px", margin: "0px" }}>Buat janji temu!</Button>
                            <Button color="white" styles={{ width: "192px", margin: "0px", color: "#00A3F4", border: "1px solid #00A3F4", marginLeft: "24px" }}>Bertemu tim kami</Button>
                        </div>
                    </div>
                    <div className="image-comp">
                        <img src="./aset/Rectangle 1982.png" alt="rectangle" className="rectangle" />
                        <img src="./aset/Group 4.png" alt="circle" className="circle" />
                        <img src="./aset/Vector.png" alt="logo-1" className="vector" />
                        <img src="./aset/Vector2.png" alt="logo-2" className="vector second" />
                        <img src="./aset/Path 1083.png" alt="logo-3" className="vector third" />
                        <img src="./aset/Ellipse 5.png" alt="spark-1" className="spark" />
                        <img src="./aset/Ellipse 7.png" alt="spark-2" className="spark second" />
                        <img src="./aset/whatsapp.png" alt="whatsapp" className="whatsapp" />
                    </div>
                </div>
            </div>
        </Fragment>
    )
}