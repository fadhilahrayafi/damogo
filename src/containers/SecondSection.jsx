import React from 'react'
import { Button } from '../components/utils'
import '../styles/secondSection.scss'

export const SecondSection = () => {
    return (
        <div className="secondContainer">
            <span className="damogo">D A M O G O</span>
            <span className="bold-title">Demo Aplikasi</span>
            <img src="./aset/ellipse 16.png" alt="blur" className="blur-effect" />
            <div className="content">
                <div className="image-comp">
                    <img src="./aset/damogo2.png" alt="display" className="display" />
                    <img src="./aset/Group 1549.png" alt="circle" className="circle" />
                    <img src="./aset/Group 1551.png" alt="blue-circle-1" className="blue-circle-1" />
                    <img src="./aset/Group 1552.png" alt="blue-circle-2" className="blue-circle-2" />
                </div>
                <div className="text-comp">
                    <span className="text">
                        <span className="bold-text">DamoGo</span> adalah sistem yang membantu proses pengadaan bahan makanan lebih mudah dan efisien!<div style={{ height: "25px" }} />
                        Atur semua pesanan dari supplier maupun bahan dari kamu ke franchise secara online.
                    </span>
                    <Button styles={{ marginTop: "16px" }} color="#00A3F4">Lihat demo<img src="./aset/Group 1547.png" alt="play" style={{ marginLeft: "9px" }} /></Button>
                </div>
            </div>
        </div>
    )
}