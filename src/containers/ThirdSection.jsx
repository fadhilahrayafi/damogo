import React from 'react'
import { Button } from '../components/utils'
import '../styles/thirdSection.scss'

export const ThirdSection = () => {
    return (
        <div className="thirdContainer">
            <div className="text-comp">
                <span className="damogo">B L O G</span>
                <span className="bold-title">Cara menyimpan daging agar awet dan segar tanpa Freezer</span>
                <span className="detail">Sudah banyak orang yang tahu jika menyimpan daging dalam freezer menjadi cara mudah menyimpan stok daging supaya awet dan tahan lama. Tapi bagaimana ya jika tidak memiliki kulkas atau freezer? Tenang! Ada kok caranya,</span>
                <span className="bold-text">yuk disimak!</span>
                <Button styles={{ marginTop: "58px" }} color="#FFBB00">Baca selengkapnya</Button>
            </div>
            <div className="image-comp">
                <div className="ellipse-yellow"></div>
                <img src="./aset/Layer 21.png" alt="food" className="food" />
                <div className="shine-comp">
                    <img src="./aset/Group 1559.png" alt="shine-1" className="shine-1" />
                    <img src="./aset/Group 1560.png" alt="shine-2" className="shine-2" />
                    <img src="./aset/Group 1561.png" alt="shine-3" className="shine-3" />
                    <img src="./aset/Group 1562.png" alt="shine-4" className="shine-4" />
                </div>
            </div>
        </div>
    )
}