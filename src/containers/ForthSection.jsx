import React from 'react'
import '../styles/forthSection.scss'

export const ForthSection = () => {
    return (
        <div className="forthContainer">
            <span className="damogo">D A M O G O</span>
            <span className="bold-title">Kerjasama</span>
            <div className="stores">
                <img style={{ width: "142.77px", height: "142.77px", marginRight: "112.76px" }} src={'./aset/alfamidi.png'} alt={"alfamidi"} />
                <img style={{ width: "199.15px", height: "99.12px", marginRight: "140.95px" }} src={'./aset/Plaza_Ambarrukmo.png'} alt={"Plaza_Ambarrukmo"} />
                <img style={{ width: "127.31px", height: "126.4px", marginRight: "127.31px" }} src={'./aset/penyetan.png'} alt={"penyetan"} />
                <img style={{ width: "142.77px", height: "142.77px" }} src={'./aset/hakata.png'} alt={"hakata"} />
            </div>
        </div>
    )
}