import React from 'react'
import { Button } from '../components/utils'
import '../styles/fifthSection.scss'

export const FifthSection = () => {
    return (
        <div className="fifthContainer">
            <div className="image-comp">
                <img className="phone" src={"./aset/damogo3.png"} alt="display" />
                <img className="ellipse" src={"./aset/ellipse 14.png"} alt="elipse" />
            </div>
            <div className="text-comp">
                <span className="damogo">D A M O G O</span>
                <span className="bold-title">Untuk Restoran</span>
                <span className="detail-text">
                    Aplikasi gratis kami adalah cara paling efisien untuk memesan
                    dari semua pemasok Anda. Selesaikan pesanan lebih cepat,
                    tidur lebih awal.
                </span>
                <Button styles={{ marginTop: "58px" }} color="#005179">Pelajari selengkapnya</Button>
            </div>
        </div>
    )
}