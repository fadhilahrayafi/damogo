import React from 'react'
import '../styles/component.scss'

export const Button = ({ children, color, styles }) => {
    return (
        <span className="button" style={{ backgroundColor: color, boxShadow: `0px 20px 30px -15px ${color}`, ...styles }}>{children}</span>
    )
}