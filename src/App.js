import './App.css';
import './main.scss'
import { FifthSection, FirstSection, ForthSection, SecondSection, SeventhSection, SixthSection, ThirdSection } from './containers';

function App() {
  return (
    <div className="App">
      <div className="body">
        <FirstSection />
        <SecondSection />
        <ThirdSection />
        <ForthSection />
        <FifthSection />
        <SixthSection />
        <SeventhSection />
      </div>
    </div>
  );
}

export default App;
